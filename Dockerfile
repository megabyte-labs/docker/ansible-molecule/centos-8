FROM centos:8
LABEL maintainer="help@megabyte.space"

ENV container docker

# Source: https://github.com/geerlingguy/docker-centos8-ansible/blob/master/Dockerfile
# Source: https://github.com/j8r/dockerfiles/blob/master/systemd/centos/8.Dockerfile
# Source: https://hub.docker.com/_/centos/

SHELL ["/bin/bash", "-eo", "pipefail", "-c"]
# hadolint ignore=DL3003
RUN set -ex \
  && (cd /lib/systemd/system/sysinit.target.wants/; \
      for i in *; do [ "$i" == systemd-tmpfiles-setup.service ] || rm -f "$i"; done); \
      rm -f /lib/systemd/system/multi-user.target.wants/*; \
      rm -f /etc/systemd/system/*.wants/*; \
      rm -f /lib/systemd/system/local-fs.target.wants/*; \
      rm -f /lib/systemd/system/sockets.target.wants/*udev*; \
      rm -f /lib/systemd/system/sockets.target.wants/*initctl*; \
      rm -f /lib/systemd/system/basic.target.wants/*; \
      rm -f /lib/systemd/system/anaconda.target.wants/*; \
  yum -y install \
      python36-3.* \
      sudo-1.* \
  && yum clean all \
  && sed -i -e 's/^\(Defaults\s*requiretty\)/#--- \1/'  /etc/sudoers \
  && mkdir -p /etc/ansible \
  && echo -e '[local]\nlocalhost ansible_connection=local' > /etc/ansible/hosts \
  && groupadd -r ansible \
  && useradd -m ansible -p ansible -g ansible \
  && usermod -aG wheel ansible \
  && sed -i "/^%wheel/s/ALL\$/NOPASSWD:ALL/g" /etc/sudoers

VOLUME ["/sys/fs/cgroup", "/tmp", "/run"]

CMD ["/usr/lib/systemd/systemd"]
